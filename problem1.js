const fs = require('fs');

//Problem1
const loginFunction = () => {
    return new Promise((resolve, reject) => {
        const userId = process.argv[2]
        const type = process.argv[3] ? process.argv[3] : 0
        if (userId && userId.length < 8) {
            const uniqueId = Math.ceil(Math.random() * 10000000000)
            resolve({ uuid: uniqueId, type: type })
        } else {
            reject({
                response: 400,
                reason: "BadRequest"
            })
        }

    })
}

//function to read file
const readFile = async (filename) => {
    const data = await fs.promises.readFile(filename)
    return JSON.parse(data)
}

//function to addItemToCart
const addItemToCart = async (userId, itemToAdd) => {
    const cartData = await readFile("Question/cart.json")
    return new Promise((resolve, reject) => {
        if (userId) {
            if (cartData[userId]) {
                cartData[userId] = [...cartData[userId], itemToAdd]
            } else {
                cartData[userId] = [itemToAdd]
            }
            fs.promises.writeFile("Question/cart.json", JSON.stringify(cartData))
                .then(() => resolve(`${itemToAdd.name} added to cart and cart updated successfully`))
        } else {
            reject({ code: 400, reason: "BadRequest" })
        }
    })
}

//Problem2
const getProductAndAddItemToCart = async (userId) => {
    const data = await readFile("Question/products.json")
    const userIdArr = Object.keys(data)
    const randomNumber = Math.floor(Math.random() * userIdArr.length)
    const _id = userIdArr[randomNumber]
    const product = data[_id]
    const itemToAdd = { _id, ...product, quantity: 1 }
    return addItemToCart(userId, itemToAdd)

}


//Problem3
const findExpensiveItemAndAddToCart = async (userId) => {
    const priceDetails = await readFile("Question/price.json")
    const _id = Object.keys(priceDetails).reduce((res, curr) => {
        const item1 = priceDetails[res]
        const item2 = priceDetails[curr]
        if (item1.price - item1.discount < item2.price - item2.discount) {
            return curr
        }
        return res
    })
    const products = await readFile("Question/products.json")
    const costlyItem = products[_id]
    const itemObj = { _id, ...costlyItem, quantity: 1 }
    return addItemToCart(userId, itemObj)
}


//Problem4
const extractItemsWithNameAndPrice = async () => {
    const priceData = await readFile("Question/price.json")
    const productsData = await readFile("Question/products.json")
    return new Promise((resolve, reject) => {
        const updatedData = Object.keys(productsData).reduce((res, curr) => {
            const data = `${productsData[curr].name} - ${priceData[curr].price}\n`
            res += data
            return res
        }, "")
        fs.promises.writeFile("itemNameAndPrice.json", updatedData)
            .then(() => resolve("New file with item name and price created successfully"))
    })
}

//Problem6
// const addItemToCatalog = (userId)=>{
// }

(async function () {
    try {
        const user = await loginFunction()
        console.log(user)
        const res = await getProductAndAddItemToCart(user.uuid)
        console.log(res)
        const res2 = await findExpensiveItemAndAddToCart(user.uuid)
        console.log(res2)
        const res3 = await extractItemsWithNameAndPrice()
        console.log(res3)
    } catch (e) {
        console.log(e)
    }
})()